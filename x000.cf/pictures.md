# Sample pictures

![photo1](photo1.jpeg)

<hr>

![profile](profile.png)

<hr>

* [Kubernetes in action](Marko%20Luksa%20-%20Kubernetes%20in%20Action%20(2018%2C%20Manning%20Publications).pdf)
* [001 Why Use Docker](001%20Why%20Use%20Docker.mp4)
* [009 But Really...Whats a Container.mp4](009%20But%20Really...Whats%20a%20Container.mp4)
* [HugeVideo.mp4](hugevideo.mp4)